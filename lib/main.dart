import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rest_api_lessons/data/api_services/post_api_service.dart';
import 'package:rest_api_lessons/utils/locale.dart';
import 'package:rest_api_lessons/widgets/bottom_nav.dart';

void main() {
  Get.put(PostApiService());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      translations: RestApiLanguage(),
      locale: const Locale('en', 'US'),
      debugShowCheckedModeBanner: false,
      home: const BottomNav(),
    );
  }
}
