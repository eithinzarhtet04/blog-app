import 'package:get/get.dart';

class RestApiLanguage extends Translations{
  @override
  // TODO: implement keys
  Map<String, Map<String, String>> get keys => {
    'en_US': {
      'home': 'Home',
      'upload': 'Upload',
      'settings': 'Settings'
    },
    'en_MM': {
      'home': 'မူလစာမျက်နှာ',
      'upload': 'တင်မည်',
      'settings':'ပြင်ဆင်ရန်'
    }
  };

}