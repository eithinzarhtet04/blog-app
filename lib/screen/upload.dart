import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart' as d;
import 'package:rest_api_lessons/controller/upload_post_controller.dart';

class UploadScreen extends StatefulWidget {
  const UploadScreen({super.key});

  @override
  State<UploadScreen> createState() => _UploadScreenState();
}

class _UploadScreenState extends State<UploadScreen> {
  UploadPostController _uploadPostController = Get.put(UploadPostController());
  GlobalKey<FormState> _key = GlobalKey();
  ImagePicker _imagePicker = ImagePicker();
  File? file;
  String?_title, _body;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Upload Post'),
        centerTitle: true,
      ),
      body: Obx((){
        UploadPostState uploadPostState = _uploadPostController.uploadPostState.value;
        if(uploadPostState is UploadPostLoading){
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Updating..... ${(_uploadPostController.percentage * 100).toInt()}'),
                const Divider(),
                CircularProgressIndicator(value: (_uploadPostController.percentage * 100),)
              ],
            )
          );
        }else if(uploadPostState is UploadPostSuccess){
          final result = uploadPostState.uploadResponse.result ?? '';
          return Center(child: Text(result));
        }else if(uploadPostState is UploadPostError){
          return const Center(child: Text('Something wrong'));
        }
        return Form(
          key: _key,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
              children: [
                const Text('Enter title'),
                TextFormField(
                  onSaved: (str){
                    _title = str;
                  },
                  validator: (str){
                    if(str == null || str.isEmpty){
                      return 'Enter title';
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 5,
                ),
                const Text('Enter body'),
                TextFormField(
                  onSaved: (str){
                    _body = str;
                  },
                  validator: (str){
                    if(str == null || str.isEmpty){
                      return 'Enter body';
                    }
                    return null;
                  },
                  minLines: 3,
                  maxLines: 5,
                ),
                IconButton(
                  onPressed: () async{
                    final XFile? image = await _imagePicker.pickImage(source: ImageSource.gallery);
                    if(image != null){
                      setState(() {
                        file = File(image.path);
                      });
                    }
                  }, 
                  icon: const Icon(Icons.image)
                ),
                (file != null) ? Image.file(file!, height: 150, ) : const SizedBox(height: 10,),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton.icon(
                  onPressed: () async{
                    d.MultipartFile? multipartFile;
                    d.FormData? formData;
                    if(file != null){
                      multipartFile = await d.MultipartFile.fromFile(file!.path);
                    }
                    _key.currentState?.save();
                    if(_key.currentState != null && _key.currentState!.validate()){
                      if(multipartFile != null){
                        formData = d.FormData.fromMap({
                          'photo': multipartFile 
                        });
                      }
                      _uploadPostController.uploadPost(title: _title ?? '', body: _body ?? '', photo: formData ?? null);
                    }
                  }, 
                  icon: const Icon(Icons.upload), 
                  label: const Text('Upload')
                )
              ],),
            )
          );

      }),
    );
  }
}