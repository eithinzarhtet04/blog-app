import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rest_api_lessons/controller/blog_post_controller.dart';
import 'package:rest_api_lessons/data/api_services/post_api_service.dart';
import 'package:rest_api_lessons/data/model/blog_post_model.dart';
import 'package:shimmer/shimmer.dart';

class BlogDetailScreen extends StatefulWidget {
  final int id;
  final String title;
  BlogDetailScreen({super.key, required this.id, required this.title});

  @override
  State<BlogDetailScreen> createState() => _BlogDetailScreenState();
}

class _BlogDetailScreenState extends State<BlogDetailScreen> {
  BlogPostController _blogPostController = Get.put(BlogPostController());

  @override
  void initState() {
    super.initState();
    _blogPostController.getPost(widget.id);
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Obx((){
        BlogPostState blogPostState = _blogPostController.blogPostState.value;
        if(blogPostState is BlogPostSuccess){
          print('Success');
          BlogPost blogPost = blogPostState.post;
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 50,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(child: Text('${blogPost.title}')),
                  ),
                ),
                const Divider(),
                Container(
                  height: 100,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(child: Text('${blogPost.body}')),
                  ),
                ),
                const Divider(),
                (blogPost.photo == null) ?
                const SizedBox(height: 10,) : Image.network('${PostApiService.baseUrl}/${blogPost.photo}', height: 300,)
              ],
            ),
          );
        }
        else if(blogPostState is BlogPostLoading){
          return Shimmer.fromColors(
          baseColor: Colors.grey, 
          highlightColor: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  height: 50,
                  color: Colors.grey,
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  height: 100,
                  color: Colors.grey,
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  height: 200,
                  color: Colors.grey,
                ),
              ],
            ),
          )
        );
        }else if(blogPostState is BlogPostError){
          return const Center(child: Text('Error'));
        }
        return const Center();
      }),
    );
  }
}