import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rest_api_lessons/controller/post_list_controller.dart';
import 'package:rest_api_lessons/widgets/post_list_widget.dart';
import 'package:shimmer/shimmer.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final PostListController _postListController = Get.put(PostListController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Post List'),
        centerTitle: true,
      ),
      body: Obx((){
        PostListState _postListState = _postListController.postListState.value;
        print(_postListState);
        if(_postListState is PostListLoading){
          return ListView(
            children: [
              for(int i=1; i<=15; i++)
                Shimmer.fromColors(
                  baseColor: Colors.grey, 
                  highlightColor: Colors.grey,
                  child: Container(
                    height: 30,
                    margin: const EdgeInsets.all(8.0),
                    color: Colors.grey,
                  )
                )
            ],
          );
        }else if(_postListState is PostListSuccess){
          final posts = _postListState.postList;
          return PostListWidget(posts: posts);
        }else if(_postListState is PostListError){
          return const Text('Error');
        }else{
          return const Center();
        }
      })
    );
  }
}