import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _darkTheme = false;
  bool _mmTheme = false;
  @override
  Widget build(BuildContext context) {
    Brightness brightness = Theme.of(context).brightness;
    _darkTheme = (brightness == Brightness.dark);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Card(
            child: ListTile(
              leading: Text('Dark Theme'),
              trailing: Switch(
                value: _darkTheme, 
                onChanged: (value){
                  if(value){
                    Get.changeTheme(ThemeData.dark());
                  }else{
                    Get.changeTheme(ThemeData.light());
                  }
                  setState(() {
                    _darkTheme = value;
                  });
                }
              ),
            )
          ),
          Card(
            child: ListTile(
              leading: Text('မြန်မာဘာသာသို့ပြောင်းရန်'),
              trailing: Switch(
                value: (_mmTheme), 
                onChanged: (value){
                  if(value){
                    Get.updateLocale(Locale('en','MM'));
                  }else{
                    Get.updateLocale(Locale('en', 'US'));
                  }
                  setState(() {
                    _mmTheme = value;
                  });
                }
              ),
            ),
          )
        ],
      ),
    );
  }
}