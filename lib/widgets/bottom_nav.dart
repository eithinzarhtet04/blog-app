import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rest_api_lessons/screen/home.dart';
import 'package:rest_api_lessons/screen/settings.dart';
import 'package:rest_api_lessons/screen/upload.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({super.key});

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _currentIndex = 0;
  Widget _body = Home();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(icon: const Icon(Icons.home), label: 'home'.tr),
          BottomNavigationBarItem(icon: const Icon(Icons.add), label: 'upload'.tr),
          BottomNavigationBarItem(icon: const Icon(Icons.settings), label: 'settings'.tr),
        ],
        onTap: (int index){
          setState(() {
            _currentIndex = index;
            if(index == 0){
              _body = Home();
            }else if(index == 1){
              _body = const UploadScreen();
            }else if(index == 2){
              _body = const SettingsScreen();
            }
          });
        },
      ),
    );
  }
}