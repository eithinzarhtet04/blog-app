import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rest_api_lessons/data/model/posts_list_model.dart';
import 'package:rest_api_lessons/screen/blog_detail.dart';

class PostListWidget extends StatelessWidget {
  final List<PostListModel> posts;
  PostListWidget({super.key, required this.posts});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
              itemCount: posts.length,
              itemBuilder: (context, index){
                return InkWell(
                  onTap: (){
                    Get.to(BlogDetailScreen(id: posts[index].id ?? 0, title: posts[index].title ?? '',));
                  },
                  child: Card(child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(posts[index].title ?? ''),
                  )),
                );
              }
            );
  }
}