import 'package:get/get.dart';
import 'package:rest_api_lessons/data/api_services/post_api_service.dart';
import 'package:rest_api_lessons/data/model/posts_list_model.dart';

class PostListController extends GetxController{
  PostApiService _postApiService = Get.find();
  Rx<PostListState> postListState = PostListState().obs;

  void getAllPosts(){
    postListState.value = PostListLoading();
    _postApiService.getAllPosts()
    .then((postList) {
      postListState.value = PostListSuccess(postList);
    })
    .catchError((value){
      postListState.value = PostListError();
    });
  }

  @override
  void onInit() {
    super.onInit();
    getAllPosts();
  }
}
class PostListState{}
class PostListLoading extends PostListState{}
class PostListSuccess extends PostListState{
  final List<PostListModel> postList;
  PostListSuccess(this.postList);
}
class PostListError extends PostListState{}