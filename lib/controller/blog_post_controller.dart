import 'package:get/get.dart';
import 'package:rest_api_lessons/data/api_services/post_api_service.dart';
import 'package:rest_api_lessons/data/model/blog_post_model.dart';

class BlogPostController extends GetxController{
  final PostApiService _apiService = Get.find();
  Rx<BlogPostState> blogPostState = BlogPostState().obs;

  void getPost(int id){
    blogPostState.value = BlogPostLoading();
    _apiService.getPost(id)
    .then((posts){
      print(posts);
      if(posts.isNotEmpty) {
        blogPostState.value = BlogPostSuccess(post: posts[0]);
      }
    })
    .catchError((onError){
      print(onError);
      blogPostState.value = BlogPostError();
    });
  }
}

class BlogPostState{}
class BlogPostLoading extends BlogPostState{}
class BlogPostSuccess extends BlogPostState{
  final BlogPost post;

  BlogPostSuccess({required this.post});
}
class BlogPostError extends BlogPostState{}