import 'package:get/get.dart';
import 'package:rest_api_lessons/controller/post_list_controller.dart';
import 'package:rest_api_lessons/data/api_services/post_api_service.dart';
import 'package:dio/dio.dart' as dio;
import 'package:rest_api_lessons/data/model/upload_response.dart';
import 'package:rest_api_lessons/widgets/bottom_nav.dart';

class UploadPostController extends GetxController{
  final PostApiService _postApiService = Get.find();
  PostListController postListController = Get.find();
  Rx<UploadPostState> uploadPostState = UploadPostState().obs;
  RxDouble percentage = 0.0.obs;
  void uploadPost({required String title, required String body, required dio.FormData? photo}){
    uploadPostState.value = UploadPostLoading();
    _postApiService.uploadPost(title: title, body: body, photo: photo,
    uploadProgress: (send, data) {
      percentage.value = send / data;
    },)
    .then((post){
      uploadPostState.value = UploadPostSuccess(post);
      Future.delayed(const Duration(seconds: 1))
      .then((value){
        Get.offAll(const BottomNav());
        postListController.getAllPosts();
        uploadPostState.value = UploadPostState();
      });
    })
    .catchError((onError){
      uploadPostState.value = UploadPostError();
    });
  }
}
class UploadPostState{}
class UploadPostLoading extends UploadPostState{}
class UploadPostSuccess extends UploadPostState{
  final UploadResponse uploadResponse;

  UploadPostSuccess(this.uploadResponse);
}
class UploadPostError extends UploadPostState{}