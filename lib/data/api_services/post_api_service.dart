import 'package:dio/dio.dart';
import 'package:rest_api_lessons/data/model/blog_post_model.dart';
import 'package:rest_api_lessons/data/model/posts_list_model.dart';
import 'package:rest_api_lessons/data/model/upload_response.dart';

class PostApiService{
  static const baseUrl = 'http://44.207.161.4:5000';
  Dio _dio = Dio();

  Future<List<PostListModel>> getAllPosts() async{
    final result = await _dio.get('$baseUrl/posts');
    List postList = result.data as List;
    return postList.map((post){
      return PostListModel.fromJson(post);
    }).toList();
  }

  Future<List<BlogPost>> getPost(int id) async{
    var result = await _dio.get('$baseUrl/post?id=$id');
    List postList = result.data as List;
    return postList.map((post){
      return BlogPost.fromJson(post);
    }).toList();
  }

  Future<UploadResponse> uploadPost({required String title, required String body, required FormData? photo, required Function(int,int) uploadProgress}) async{
    var result = await _dio.post('$baseUrl/post?title=$title&body=$body', data: photo, onSendProgress: uploadProgress);
    UploadResponse uploadResponse = UploadResponse.fromJson(result.data);
    return uploadResponse;

  }
}