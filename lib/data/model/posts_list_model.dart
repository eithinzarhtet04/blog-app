class PostListModel{
  int? id;
  String? title;

  PostListModel({int? id1, String? title1}){
    this.id = id1;
    this.title = title1;
  }

  factory PostListModel.fromJson(Map<String,dynamic> data){
    int? id2 = data['id'];
    String? title2 = data['title'];

    return PostListModel(
      id1: id2,
      title1: title2
    );
  }
}
